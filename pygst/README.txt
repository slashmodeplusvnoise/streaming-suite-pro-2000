running gstreamer pipeles controllable via OSC

USEAGE:
./streamer.py <pipelinefile>
this will run the pipeline specified by <pipelinefile> and create an 
OSC-server listening on UDP:localhost:9001


for each _named_ element in the pipeline the script will create an 
OSC-receiver.
e.g. if your pipeline looks like "videotestsrc name=bla ! xvimagesink",
this will create exactly one OSC-listener "/bla".

you can then control this element by sending messages to this receiver:
e.g "/OSC pattern 10" (OSC-selector being "/OSC", the first element being
a symbol 'pattern' and the 2nd argument
is a number "10")
the first argument ('pattern') is the property-name of the element
the 2nd argument is the new value of this property

currently there is no way to query a value
