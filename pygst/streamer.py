#!/usr/bin/env python
# vim: set fileencoding=UTF-8 :


## this application only runs because the osc-thread has not yet terminated.
## as soon as we stop this (by doing osc.dontListen()) the app terminates


import sys, os, thread, time
import pygst
pygst.require("0.10")
import gst
import osc
from threading import Thread
import signal
import string
from time import strftime
import subprocess


class Watcher:
    """this class solves two problems with multithreaded
    programs in Python, (1) a signal might be delivered
    to any thread (which is just a malfeature) and (2) if
    the thread that gets the signal is waiting, the signal
    is ignored (which is a bug).

    The watcher is a concurrent process (not thread) that
    waits for a signal and the process that contains the
    threads.  See Appendix A of The Little Book of Semaphores.
    http://greenteapress.com/semaphores/

    I have only tested this on Linux.  I would expect it to
    work on the Macintosh and not work on Windows.
    """
    
    def __init__(self):
        """ Creates a child thread, which returns.  The parent
            thread waits for a KeyboardInterrupt and then kills
            the child thread.
        """
        self.child = os.fork()
        if self.child == 0:
            return
        else:
            self.watch()

    def watch(self):
        try:
            os.wait()
        except KeyboardInterrupt:
            # I put the capital B in KeyBoardInterrupt so I can
            # tell when the Watcher gets the SIGINT
            print 'KeyBoardInterrupt'
            self.kill()
        sys.exit()

    def kill(self):
        try:
            os.kill(self.child, signal.SIGKILL)
        except OSError: pass

class ClockThread(Thread) :
	def __init__(self, clock) :
	   Thread.__init__(self)
	   self.clock=clock

	def run(self) :
	  if self.clock :
            self.isRunning = True
	    while self.isRunning :
		datestring=time.strftime("%H:%M", time.localtime())
		self.clock.set_property("text", datestring)
		time.sleep(1)

        def stop(self) :
		self.isRunning = False
		self.join(10)
              
def pipeRead(pipefile='pipelines/default.txt', mountpoint='stream', filename=None):
	if pipefile==None:
		print "no pipefile specified for pipe"
	if mountpoint==None:
		mountpoint='stream'
	mountpoint=str(mountpoint)
	print "eat file", pipefile
	try: f=open(pipefile, 'r')
	except Exception, x:
		print "couldn't open file: ", x
		return
	try: data=f.read()
	except Exception, x:
		print "error reading file: ", pipefile
		print "exception: ", x
		return
	try: f.close()
	except Exception, x:
		print "error closing file: ", pipefile
		print "exception: ", x
		return
	## replace some macros
	if data!=None:
		data=data.replace('@mountname@', mountpoint)
		if filename==None:
			filename="stream-"+time.strftime("%Y%m%d-%H%M%S")
		filename=filename+".dv"
		data=data.replace('@filename@', filename)
	return data


class Main():
  dvgrab = 0
  def __del__(self):
     if (osc != None):
	     osc.dontListen()
     if (self.clockthread != None):
	     self.clockthread.stop()
     dvgrab.terminte()

  def __init__(self, filename, event, name, args=None):
    self.clockthread=None
    pipestring = pipeRead(pipefile=filename)
    
    if pipestring == None:
	print "failed to read pipeline...exiting"
	return

    ctime = strftime("%Y-%m-%d_%H:%M:%S")

    # substitute the template values with the real deal
    pipestring = string.replace(pipestring,"EVENT",event)
    pipestring = string.replace(pipestring,"NAME",name)
    pipestring = string.replace(pipestring,"TIMESTAMP",ctime)

    self.pipeline1 = gst.parse_launch(pipestring)
    print "pipeline: ", pipestring

    dvgrab = subprocess.Popen(['dvgrab', '/media/chmodx/'+event+'-stream-'+name+'-'+ctime+'-.dv'])
    
    self.clock = self.pipeline1.get_by_name('clock')
    self.clockthread = ClockThread(self.clock)
    self.clockthread.start()

    def controlMess(*msg):
	try:
		ctl=msg[0]
		print "control: ", ctl
		elementname=ctl.pop(0)[1:]
		typs=ctl.pop(0)
		propertyname=ctl.pop(0)
		value=ctl.pop(0)
		if (type(value)==str):
			value=value.replace("&", "&amp;")
		try:
			element=self.pipeline1.get_by_name(elementname)
			if element != None:
				element.set_property(propertyname, value)
			else:
				print "no element of name ", elementstring
		except Exception, x:
			print "cought exception: ", x
		except:
			print "something went wrong"
	except Exception, x:
		print "controlMess exception: ", x
	except:
		print "something went wrong in controlMess"

    def quitMess(*msg):
	osc.doneListen()

    osc.init()
    
    ## osc.createListener() # this defaults to port 9001 as well
    osc.listen('0.0.0.0', 9001)

    # bind addresses to functions -> textMess() function will be triggered everytime a
    # "/test" labeled message arrives
    osc.bind(quitMess, "/quit")

    for token in pipestring.split():
	if token.startswith("name="):
		token=token[5:]
		print "binding '%s' to controlMess" % (token)
		osc.bind(controlMess, "/"+token)

    print 'ready to receive osc messages ...'
    self.pipeline1.set_state(gst.STATE_READY)
    self.pipeline1.set_state(gst.STATE_PLAYING)
    print 'starting streaming pipeline ...'

def main(script, mount=None, file=None, event=None, name=None, *args):
	print "main starting"
        try:
		print "start watcher"
                Watcher()
                start=Main(mount, file, event, name)
        except Exception, x:
                print "caught exception ", x



if __name__ == '__main__':
    main(*sys.argv)

