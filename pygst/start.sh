#!/bin/sh

cd ${0%/*}

if [ "x${STREAMERARGS}" = "x" ]; then
 STREAMERARGS=$@
fi

./streamer.py $STREAMERARGS
