#!/usr/bin/env python
# r3340 - an OSC IRC bot that want to take over the world
# DEPENDS: python-liblo, python-irclib

import liblo, sys
from cervelle import cervelle
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# send to OSC server <port> on localhost
# joins <channel> on <server>, as <nickname>
if len(sys.argv) == 6:
    (nickname, server, channel, hostname, ports) = (sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
else:
    print '\033[1;31mUsage:\033[m r3340.py <nickname> <server> <irc-channel> <OSC target IP/hostname> <OSC port(s)>'
    print '\033[1;32mexample:\033[m ./r3340.py r3340 irc.goto10.org mode+v localhost 57120'
    print '\033[1;32mexample:\033[m ./r3340.py banana localhost awesome 10.0.0.18 9997,9998,9999'
    sys.exit(1)

try:
    targets = {}
    for port in ports.split(','):
        targets[port] = liblo.Address(hostname,port)

except liblo.AddressError, err:
    print str(err)
    sys.exit("oioioi osc address error")

# we are the robots
class TestBot(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "I'm in #" + channel + "! :D"

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) > 1 and irc_lower(a[0]) == irc_lower(self.connection.get_nickname()):
            self.do_command(e, a[1].strip())
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection

        if cmd == "help":
            c.notice(self.channel, "feed me ASCII and I'll pass it to OSC port(s) " + ports)
        else:
            msg = liblo.Message("/titles")
            msg.add("text",nick+": "+cmd)
            for port in ports.split(','):
                liblo.send(targets[port], msg)
            print nick + ' -> ' + cmd

def main():
    bot = TestBot(channel, nickname, server, 6667)
    bot.start()

if __name__ == "__main__":
    main()
